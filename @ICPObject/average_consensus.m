function tform = average_consensus(obj, P_fg, P_bg, alpha)

if nargin < 4
    alpha = obj.alpha;
end

% Rotation
Reul_fg = rotm2eul(P_fg.R);
Reul_bg = rotm2eul(P_bg.R);
obj.R = eul2rotm(alpha*Reul_fg + (1-alpha)*Reul_bg);

% Scaling
obj.S = alpha*P_fg.S + (1-alpha)*P_bg.S;

% Translation
obj.T = alpha*P_fg.T + (1-alpha)*P_bg.T;

% Return
tform.R = obj.R;
tform.S = obj.S;
tform.T = obj.T;

end
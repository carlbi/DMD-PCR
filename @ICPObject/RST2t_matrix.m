function T_matrix = RST2t_matrix(R, S, T)
    T_matrix = ...
       [S*R(1,1) S*R(1,2) S*R(1,3) T(1);
        S*R(2,1) S*R(2,2) S*R(2,3) T(2);
        S*R(3,1) S*R(3,2) S*R(3,3) T(3);
        0        0        0        1   ];
end
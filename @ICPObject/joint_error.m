function err = joint_error(obj, P_fg, P_bg)

err = rmse([P_fg.model P_bg.model],...
        [PointcloudObject.apply_T(P_fg.data, obj.R, obj.S, obj.T) ...
        PointcloudObject.apply_T(P_bg.data, obj.R, obj.S, obj.T)]);

end
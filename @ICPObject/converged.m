function result = converged(obj)
    limit = unpack(config(), "limit");
    if numel(obj.psnr) < 2
        result = false;
    else
        result = abs(obj.error(end) - obj.error(end-1))/obj.error(end) < limit;
    end
end
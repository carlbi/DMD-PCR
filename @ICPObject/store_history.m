function store_history(obj, P_fg, P_bg)
    max_depth = max(vecnorm(P_bg.model, 2));
    error = rmse([P_fg.model P_bg.model],...
        [PointcloudObject.apply_T(P_fg.data, obj.R, obj.S, obj.T) ...
        PointcloudObject.apply_T(P_bg.data, obj.R, obj.S, obj.T)]);
    obj.error = [obj.error error];
    obj.psnr = [obj.psnr 20*log10(max_depth/error)];
    obj.residual_bg = [obj.residual_bg P_bg.residual];
    obj.residual_fg = [obj.residual_fg P_fg.residual];
    obj.r_history = [obj.r_history rotm2eul(obj.R)'];
    obj.s_history = [obj.s_history obj.S];
    obj.t_history = [obj.t_history obj.T];
    obj.alpha_history = [obj.alpha_history obj.alpha];
end
function initialize(obj, P_fg)

[n_frames, n_iter_icp] = unpack(config(), ["n_frames", "n_iter_icp"]);

disp('Initializing ICP...')

% Initialize scale to 1
obj.S = 1;

if nargin < 2
    obj.R = eye(3);
    obj.T = [0; 0; 0];
else
    Path_model = [];
    Path_data = [];
    % Compute mean foreground point
    for i = 1:n_frames
        model = reshape(P_fg.D_model(:,i), [P_fg.size_model_y, P_fg.size_model_x]);
        model_points = PointcloudObject.dep2pcl(model, "green", P_fg.K_model);
        Path_model(i,:) = mean(model_points.Location, 1);
        data = reshape(P_fg.D_data(:,i), [P_fg.size_data_y, P_fg.size_data_x]);
        data_points = PointcloudObject.dep2pcl(data, "blue", P_fg.K_data);
        Path_data(i,:) = mean(data_points.Location, 1);
    end
    % Centering
    C_data = Path_data'-mean(Path_data, 1)';
    C_model = Path_model'-mean(Path_model, 1)';
    % Check if data points available for all frames
    if sum(isnan(C_model), 'all') > 0 || sum(isnan(C_data), 'all') > 0
        obj.R = eye(3);
        obj.T = [0; 0; 0];
    else
        % Rotation
        K = C_model*C_data.';
        [U,~,V] = svd(K);
        obj.R = U*V.';
        obj.T = mean(Path_model, 1)' - mean((obj.R*Path_data'), 2);
    end
end

obj.error = [];
obj.psnr = [];
obj.residual_bg = [];
obj.residual_fg = [];
obj.r_history = [];
obj.s_history = [];
obj.t_history = [];
obj.alpha_history = [];
obj.iter_conv = n_iter_icp;

end
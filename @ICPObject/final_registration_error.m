function final_registration_error(obj, P_fg, P_bg)

P_bg.bg_association(obj, numel(P_bg.D_model));
obj.final_bg_error = rmse(P_bg.model,...
    PointcloudObject.apply_T(P_bg.data, obj.R, obj.S, obj.T));
max_bg_depth = max(vecnorm(P_bg.model, 2));
obj.final_bg_psnr = 20*log10(max_bg_depth/obj.final_bg_error);

P_fg.fg_association(obj, numel(P_fg.D_model));
obj.final_fg_error = rmse(P_fg.model,...
    PointcloudObject.apply_T(P_fg.data, obj.R, obj.S, obj.T));
max_fg_depth = max(vecnorm(P_fg.model, 2));
obj.final_fg_psnr = 20*log10(max_fg_depth/obj.final_fg_error);

end
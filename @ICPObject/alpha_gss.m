function alpha_gss(obj, P_fg, P_bg)

f_rts = @(x) obj.average_consensus(P_fg, P_bg, x);
f_err = @(tform) obj.joint_error(P_fg, P_bg);
f = @(x) f_err(f_rts(x));

left = 0;
right = 1;
tau = double((sqrt(5)-1)/2);
tol = 1e-3;

x1 = left+(1-tau)*(right-left);
x2 = left+tau*(right-left);
f_x1 = f(x1);
f_x2 = f(x2);

while (abs(right-left)>tol)
    if(f_x1<f_x2)
        right=x2;
        x2=x1;
        x1=left+(1-tau)*(right-left);
        f_x1=f(x1);
        f_x2=f(x2);
    else
        left=x1;
        x1=x2;
        x2=left+tau*(right-left);
        f_x1=f(x1);
        f_x2=f(x2);
    end
end

if(f_x1<f_x2)
    alpha = x1;
else
    alpha = x2;
end

obj.alpha = alpha;

end
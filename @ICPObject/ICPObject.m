classdef ICPObject < handle
    properties
        R
        S
        T
        t_matrix
        pc_tform
        alpha
        % History
        error
        psnr
        residual_bg
        residual_fg
        r_history
        s_history
        t_history
        alpha_history
        % Result
        iter_conv
        final_fg_error
        final_bg_error
        final_fg_psnr
        final_bg_psnr
    end
    methods
        initialize(obj, P_fg)
        alpha_gss(obj, P_fg, P_bg)
        alpha_residuals(obj, P_fg, P_bg)
        tform = average_consensus(obj, P_fg, P_bg, alpha)
        create_transforms(obj)
        store_history(obj, P_fg, P_bg)
        plot_history(obj)
        result = converged(obj)
        err = joint_error(obj, P_fg, P_bg)
    end
    methods (Static)
        pc_tform = t_matrix2pc_tform(t_matrix)
        t_matrix = RST2t_matrix(R, S, T)
    end
end
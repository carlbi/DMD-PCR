function create_transforms(obj)
    obj.t_matrix = obj.RST2t_matrix(obj.R, obj.S, obj.T);
    obj.pc_tform = obj.t_matrix2pc_tform(inv(obj.t_matrix));
end
function alpha_residuals(obj, P_fg, P_bg)

% Residuals
err_fg = P_fg.residual;
err_bg = P_bg.residual;

obj.alpha = logistic(2*(err_fg-err_bg)/(err_fg+err_bg));

end
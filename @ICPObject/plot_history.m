function plot_history(obj)

figure
subplot(3,2,1)
plot(0:obj.iter_conv, obj.r_history)
title('Rotation')
legend('Z', 'Y', 'X')
ylabel('rad')
subplot(3,2,2)
plot(0:obj.iter_conv, obj.s_history)
title('Scaling')
subplot(3,2,3)
plot(0:obj.iter_conv, obj.t_history)
title('Translation')
legend('x', 'y', 'z')
ylabel('mm')
subplot(3,2,4)
plot(1:obj.iter_conv, obj.alpha_history)
title('Alpha')
subplot(3,2,5)
plot(1:obj.iter_conv, [obj.residual_fg; obj.residual_bg])
legend('Fg', 'Bg')
ylabel('mm')
xlabel('Iteration')
title('Residuals')
subplot(3,2,6)
yyaxis left
plot(0:obj.iter_conv, obj.error)
ylabel('mm')
xlabel('Iteration')
yyaxis right
plot(0:obj.iter_conv, obj.psnr)
ylabel('PSNR (dB)')
title('Joint error')


end
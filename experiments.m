% Experiments

addpath('experiments')
addpath('utils')
addpath('utils/TFOCS-master')

%% 2.2.1.2 The missing value problem

% Runtime: ~2min
figure_2_2_1_2()

%% 2.2.1.3 Outliers and noise

% Runtime: ~2min
figure_2_2_1_3()

%% 2.2.2.1 Training and coefficient computation

% Runtime: ~4min
figure_2_2_2_1()

%% 2.2.3.2 DMD and the DL coefficient matrix

% Runtime: ~5min
figure_2_2_3_2()

%% 2.2.3.3 Separation of foreground and background

% Runtime: ~3min
figure_2_2_3_3()

%% 2.2.3.4 Review of key publications

% Runtime: ~4min
figure_2_2_3_4()

%% 2.3.3.1 Balanced residual weighting

% Runtime: ~0min
figure_2_3_3_1()

%% 3.1 Qualitative results and datasets for evaluation

% Runtime: ~2min
figure_3_1()

%% 3.2.2

% The data generation for this experiment was run on the Euler CPU Cluster
% from ETH, the data is stored in mat-files in the folder "/figure_3_2_1".
% If you want to run this experiment from scratch, and recompute the data,
% please copy the files euler_DL.m and euler_DL.m into the parent directory 
% and execute them. However, this will take a couple of hours.

% Runtime: ~15h (1.5h on 10 cores)
figure_3_2_2()

%% 3.3 Evaluation of foreground background separation

% Runtime: ~3min
figure_3_3()

%% 3.3.2 DMD separation

% Runtime: ~3min per dataset
figure_3_3_2()

%% 3.3.3 DMD separation in the presence of noise

% Runtime: ~9min
figure_3_3_3()

%% 3.3.4 Truncated DMD reconstruction

% Runtime: ~5min
figure_3_3_4()

%% 3.4.2 Comparison weighting scheme

% Runtime: ~5min
figure_3_4_2()

%% 3.4.3 Comparison to standard methods

% Runtime: ~4min
figure_3_4_3()

%% 3.4.4 Influence of initialization

% Runtime: ~90min
figure_3_4_4()



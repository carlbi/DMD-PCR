### Dynamic Mode Decomposition for Point Cloud Registration

*Master Thesis ETH Zurich MSc Robotics, January 2021*

*Carl Philipp Biagosch*



##### How to execute the code

Run the script main.m in order to execute the whole pipeline.

If you wish to exclude steps of the pipeline (such as dictionary learning or stabilization of modes), please execute section-wise. 



##### Reproducing figures of the code

Run the sections of the script experiments.m in order to reproduce the figures from the report.

All functions for the figures are named matching the report and are located in the "/experiments" folder.



##### Implementation details

The code is structured using four main classes:

- <u>DepthmapObject</u>: Containing all methods working on the depthmaps, including DMD.

- <u>DictionaryObject</u>: Containing methods to train a dictionary, as well as computing optimal

  coefficients and reconstruction.

- <u>PointcloudObject</u>: Containing all methods working on and between point clouds, such as closest point or optimal transformation.

- <u>ICPObject</u>: Containing methods to run and record the iterations of the consensus-based ICP, as well as methods to achieve consensus and compute the weighting.

The file config.m can be used to manipulate the parameters of the implementation, as well as select the dataset used for execution. 

The raw data is stored in the "/data" folder, calibration files in the "/calib" folder.

All optimization problems are solved using standard or custom solvers from or based on the TFOCS package.


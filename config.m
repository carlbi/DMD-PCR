function conf = config()
%% Configuration parameters
    % Depthmap Parameters
    conf.n_frames = 200;
    conf.compression = 6;
    conf.fps = 15;
    conf.dt = 1/conf.fps;
    conf.t = linspace(0,conf.n_frames/conf.fps,conf.n_frames); 
    % Dictionary parameters
    conf.p_size = 8; % patch size
    conf.n_atoms = conf.p_size^2; % No. of dictionary atoms
    conf.no_patches_train = 5000;
    conf.lambda_DL = 1e-5;
    % DMD parameters
    conf.tol = 0.1;
    conf.r = conf.n_frames - 5;
    conf.filter = false;
    % ICP parameters
    conf.n_points = 1e4;
    conf.n_iter_icp = 40;
    conf.limit = 1e-3;

%% Large Room Dataset 1 Person
    % Cameras
    conf.cameras = ["22162", "26382401"];
    % Filepaths
    conf.dep = ["data/Large Room Dataset/184223/22162/depth",...
    "data/Large Room Dataset/184223/26382401/depth"];
    conf.rgb = ["data/Large Room Dataset/184223/22162/rgb",...
    "data/Large Room Dataset/184223/26382401/rgb"];
    conf.calib = ["calib/SN22162.txt",...
    "calib/SN26382401.txt"];
    % Labeling large room (view 2)
    conf.X_lims = [-1200, 400];
    conf.Y_lims = [-300, 620];
    conf.Z_lims = [800, 1750];
     
%% Large Room Dataset 2 People
%     % Cameras
%     conf.cameras = ["22162", "26382401"];
%     % Filepaths
%     conf.dep = ["data/Large Room Dataset/184428/22162/depth",...
%     "data/Large Room Dataset/184428/26382401/depth"];
%     conf.rgb = ["data/Large Room Dataset/184428/22162/rgb",...
%     "data/Large Room Dataset/184428/26382401/rgb"];
%     conf.calib = ["calib/SN22162.txt",...
%     "calib/SN26382401.txt"];
%     % Labeling large room (view 2)
%     conf.X_lims = [-1200, 400];
%     conf.Y_lims = [-300, 620];
%     conf.Z_lims = [800, 1750];

%% Small Room Dataset 1 Person
%     % Cameras
%     conf.cameras = ["22163", "22164"];
%     % Filepaths
%     conf.dep = ["data/Small Room Dataset/142638/22163/depth",...
%     "data/Small Room Dataset/142638/22164/depth"];
%     conf.rgb = ["data/Small Room Dataset/142638/22163/rgb",...
%     "data/Small Room Dataset/142638/22164/rgb"];
%     conf.calib = ["calib/SN22163.txt",...
%     "calib/SN22164.txt"];
%     % Labeling small room (view 1)
%     conf.X_lims = [-300, 400];
%     conf.Y_lims = [-350, 300];
%     conf.Z_lims = [300, 700];
%     conf.Circle = [60 550];
%     conf.radius = 300;

%% Small Room Dataset 2 People
%     % Cameras
%     conf.cameras = ["22163", "22164"];
%     % Filepaths
%     conf.dep = ["data/Small Room Dataset/143327/22163/depth",...
%     "data/Small Room Dataset/143327/22164/depth"];
%     conf.rgb = ["data/Small Room Dataset/143327/22163/rgb",...
%     "data/Small Room Dataset/143327/22164/rgb"];
%     conf.calib = ["calib/SN22163.txt",...
%     "calib/SN22164.txt"];
%     % Labeling small room (view 1)
%     conf.X_lims = [-300, 400];
%     conf.Y_lims = [-350, 300];
%     conf.Z_lims = [300, 700];
%     conf.Circle = [60 550];
%     conf.radius = 300;



end
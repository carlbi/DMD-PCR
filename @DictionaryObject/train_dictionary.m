function train_dictionary(obj)

[p_size, n_atoms] = unpack(config(), ...
    ["p_size", "n_atoms"]);

% Start with randomly generated dictionary
D = rand(p_size*p_size,n_atoms);
patches = obj.training_data;
% Options for TFOCS solver
opts.maxIts = 200;
opts.alg = 'GRA';
opts.printEvery = 0;
if obj.nonneg == true
    opts.nonneg = true;
end

err= [];
sparsity = [];
max_iter = 10; % Number of iterations to learn dictionary

disp('Training dictionary...')
for i = progress(1:max_iter)
    % Alternate Optimization
    [B,~,~] = solver_L1RLS(linop_matrix(D','R2R'), patches, obj.lambda_DL, [], opts);
    [D,~,~] = solver_L1RLS(linop_matrix(B.','R2R'), patches.', 1e-12, [], opts);
    % Store error and sparsity
    fn = norm((patches-D'*B));
    sp = sum(B==0, 'all')/numel(B);
    err = [err fn];
    sparsity = [sparsity sp];
    % Stopping criterion
    if(fn<1e-6)
        disp('Exit: Residual smaller then 1e-3')
        break; 
    end
end

% Store result
obj.Dict = D';
obj.Betas = B;
obj.training_error = err;
obj.training_sparsity = sparsity;

end
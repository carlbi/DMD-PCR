function plot_dictionary(obj, scaler)

p_size = unpack(config(), "p_size");

if nargin < 2
    scaler = 1;
end

figure
for i = 1:p_size^2
   subplot(p_size,p_size,i)
   if scaler == -1
       imshow(rescale(reshape(obj.Dict(:,i), [p_size p_size])))
   else
       imshow(scaler*reshape(obj.Dict(:,i), [p_size p_size]))
   end
end 

end
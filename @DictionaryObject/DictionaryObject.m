classdef DictionaryObject < handle
    properties
        Dict
        Betas
        training_data
        training_error
        training_sparsity
        % Controls
        nonneg
        lambda_DL
    end
    methods
        function obj = DictionaryObject()
            obj.lambda_DL = unpack(config(), "lambda_DL");
            obj.nonneg = false;
        end
        get_training_data(obj, depthmap)
        train_dictionary(obj)
        train_dictionary_ns(obj)
        plot_dictionary(obj, scaler)
        plot_error(obj)
        plot_sparsity(obj)
        Beta = compute_coefficients(obj, depthmap, data)
        X_re = reconstruct_frames(obj, depthmap, data)
    end
    methods (Static)
        Dk = ns_dict_update(f, H, a, Sigma) 
    end
end
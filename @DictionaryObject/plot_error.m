function plot_error(obj)

figure
plot(obj.training_error)
xlabel("Iterations")
ylabel("Absolute L2 error")
title("Training error")

end
function get_training_data(obj, depthmap)

[n_frames, p_size, no_patches_train] = unpack(config(),...
    ["n_frames", "p_size", "no_patches_train"]);

% Generate 3 random frames 
r = ceil(n_frames.*rand(3,1));
img = [reshape(depthmap.X(:,r(1)), [depthmap.size_y, depthmap.size_x]) ...
       reshape(depthmap.X(:,r(2)), [depthmap.size_y, depthmap.size_x]) ...
       reshape(depthmap.X(:,r(3)), [depthmap.size_y, depthmap.size_x])]; 
% Generate patches from image 
all_patches = im2col(img,[p_size p_size],'sliding');
% Delete empty patches
all_patches(:,sum(all_patches==0,1) > size(all_patches,1)/3) = [];
all_patches(:,sum(all_patches==1,1) > size(all_patches,1)/3) = [];
% Sample random patches for training
patches = all_patches(:,randperm(no_patches_train));
% Normalize patches
% obj.training_data = patches/max(patches(:));
obj.training_data = patches;

end


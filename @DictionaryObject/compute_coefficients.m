function Beta = compute_coefficients(obj, D, data)

X = D.(data);

[n_frames, p_size, n_atoms] = unpack(config(),...
    ["n_frames", "p_size", "n_atoms"]);

opts.maxIts = 100;
opts.alg = 'GRA';
opts.printEvery = 10;
if obj.nonneg == true
    opts.nonneg = true;
end

y = [];
for i = 1 : n_frames % Aligning patches vertically   
     temp = im2col(reshape(X(:,i), [D.size_y, D.size_x]),[p_size p_size],'distinct');  
     y  = [y temp];
end

No_patches_m = size(im2col(reshape(X(:,1), [D.size_y, D.size_x]),[p_size p_size],'distinct'), 2);

Beta=zeros(n_atoms*No_patches_m,n_frames);
disp('Calculating Beta...')
[Beta_temp,~,~] = solver_L1RLS(linop_matrix(obj.Dict,'R2R'), y, obj.lambda_DL, [], opts);
for i = 1:n_frames
    temp = Beta_temp(:,(i-1)*No_patches_m+1:i*No_patches_m);
    Beta(:,i) = temp(:);
end

% Calculate Beta frame-wise (slower)

% y = [];
% for i = 1 : n_frames % Aligning patches vertically   
%      temp = im2col(reshape(X(:,i), [D.size_y, D.size_x]),[p_size p_size],'distinct');  
%      y(:,i) = temp(:);
% end
% 
% No_patches_m = size(im2col(reshape(X(:,1), [D.size_y, D.size_x]),[p_size p_size],'distinct'), 2);
% 
% Beta=zeros(n_atoms*No_patches_m,n_frames);
% disp('Calculating Beta...')
% for i = progress(1:No_patches_m)
%     x_in = y((i-1)*(p_size.^2)+1:i*(p_size.^2),:);
%     if sum(x_in==0, 'all') == numel(x_in)
%         Beta_frame = zeros(n_atoms, size(Beta,2));
%     else
%         [Beta_frame,~,~] = solver_L1RLS(linop_matrix(obj.Dict,'R2R'), x_in, lambda_DL, [], opts);
%     end
%     Beta((i-1)*(n_atoms)+1:i*n_atoms,:) = Beta_frame;
% end

end
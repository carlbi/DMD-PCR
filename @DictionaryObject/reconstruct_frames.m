function X_out = reconstruct_frames(obj, D, data)

X_in = D.(data);

[n_frames, n_atoms, p_size] = unpack(config(), ["n_frames", "n_atoms" "p_size"]);

No_patches_m = round(size(X_in,1)/n_atoms);

X_reconstruct = zeros(size(X_in,1),n_frames);
for i = progress(1:No_patches_m)
   X_reconstruct((i-1)*(p_size.^2)+1:i*(p_size.^2),:) = obj.Dict*X_in((i-1)*(n_atoms)+1:i*(n_atoms),:);
end

for i = (1:n_frames)
  A = reshape(X_reconstruct(:,i), [p_size^2, No_patches_m]);
  temp = col2im(A,[p_size p_size],[D.size_y, D.size_x], 'distinct'); 
  % temp = real(temp/max(temp(:)));
  temp = real(temp);
  X_out(:,i) = temp(:);
end

end
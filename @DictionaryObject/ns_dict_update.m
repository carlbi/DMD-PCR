function Dk = ns_dict_update(f, D, betas, Sigma) 

% f: Patches
% H: Dictionary
% a: Coefficients

alpha = 1e-2;

Dk = D; % initialize
n_grad = 1; % number of gradient descent steps

% Taking a small step in the direction of the negative gradient  
for i = 1:n_grad
    gk = -inv(Sigma)*(f-Dk*betas)*betas.';
    Dk1 = Dk - alpha*gk; % Learning step (-=derivative/alpha)
    Dk = Dk1; % Store for next round
end

end


function train_dictionary_ns(obj)

[p_size, n_atoms] = unpack(config(), ...
    ["p_size", "n_atoms"]);

% Start with randomly generated dictionary
patches = obj.training_data;
D = rand(p_size*p_size,n_atoms);
Betas = zeros(n_atoms,size(patches,2));
% Options for TFOCS solver
opts.maxIts = 50;
opts.alg = 'GRA';
opts.printEvery = 0;
if obj.nonneg == true
    opts.nonneg = true;
end

err= [];
sparsity = [];
max_iter = 10; 

for i = (1:max_iter)
    fprintf('Training dictionary %dst loop...', i)
    for j = progress(1:size(patches, 2))
        % Alternate Optimization
        [Betas(:,j),~,~,Sigma] = solver_DL(D, patches(:,j), obj.lambda_DL, [], opts);
        D = obj.ns_dict_update(patches(:,j), D, Betas(:,j), Sigma);    
    end
    % Store error and sparsity
    fn = norm((patches-D*Betas));
    sp = sum(Betas==0, 'all')/numel(Betas);
    err = [err fn];
    sparsity = [sparsity sp];
    % Stopping criterion
    if(fn<1e-6)
        disp('Exit: Residual smaller then 1e-3')
        break; 
    end
end

% Store result
obj.Dict = D;
obj.Betas = Betas;
obj.training_error = err;
obj.training_sparsity = sparsity;

end
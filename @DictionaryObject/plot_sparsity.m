function plot_sparsity(obj)

figure
plot(obj.training_sparsity)
xlabel("Iterations")
ylabel("Number of zero entries")
title("Sparsity")

end
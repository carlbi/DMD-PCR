function get_depthmaps(obj, Model, Data)
    if obj.agent == "Foreground"
        if isempty(Model.Mr_fg)
            obj.D_model = Model.X.*Model.Mx_fg;
            obj.D_data = Data.X.*Data.Mx_fg;
        else
            obj.D_model = Model.R.*Model.Mr_fg;
            obj.D_data = Data.R.*Data.Mr_fg;
        end
    elseif obj.agent == "Background"
        if isempty(Model.R_bg)
            obj.D_model = Model.X_bg(:,1);
            obj.D_data = Data.X_bg(:,1);
        else
            obj.D_model = Model.R_bg(:,1);
            obj.D_data = Data.R_bg(:,1);
        end
    else
        error('Could not determine if agent is foreground or background')
    end
    % sizes for reconstruction
    obj.size_model_x = Model.size_x;
    obj.size_model_y = Model.size_y;
    obj.size_data_x = Data.size_x;
    obj.size_data_y = Data.size_y;
    % calibration
    obj.K_model = Model.K;
    obj.K_data = Data.K;
end
function plot_registration_result2D(P_fg, P_bg, T)

show_frame = 60;

PC1 = PointcloudObject.dep2pcl(...
    reshape(P_bg.D_model, [P_bg.size_model_y, P_bg.size_model_x]), ...
    "cyan", P_bg.K_model);
PC2 = pctransform(PointcloudObject.dep2pcl(...
    reshape(P_bg.D_data, [P_bg.size_data_y, P_bg.size_data_x]), ...
    "magenta", P_bg.K_data), T.pc_tform); 
PC3 = PointcloudObject.dep2pcl(...
    reshape(P_fg.D_model(:,show_frame), [P_fg.size_model_y, P_fg.size_model_x]), ...
    "blue", P_fg.K_model);
PC4 = pctransform(PointcloudObject.dep2pcl(...
    reshape(P_fg.D_data(:,show_frame), [P_fg.size_data_y, P_fg.size_data_x]), ...
    "red", P_fg.K_data), T.pc_tform); 
PC_combined = PointcloudObject.combine_pointclouds([PC1, PC2, PC3, PC4]);

% for v = 1:4
%     subplot(2,2,v)
%     hold on
%     pcshow(PC_combined)
%     switch v
%         case 1
%             view([0 -0.1 -1])
%         case 2
%             view([0 -0.3 -1])
%         case 3
%             view([0.5 -0.1 -1])
%             camroll(-90)
%         case 4
%             view([-0.5 -0.1 -1])
%             camroll(90)
%     end
%     set(gca, 'XColor', [0.5 0.5 0.5], 'YColor', [0.5 0.5 0.5], 'ZColor', [0.5 0.5 0.5])
% end

pcshow(PC_combined)
ylim([-2000 2000])
view([0 -0.3 -1])

end
function PC_out = combine_pointclouds(PC_in)
    n_in = numel(PC_in);
    Location = [];
    Color = [];
    for i = 1:n_in
        Location = [Location; PC_in(i).Location];
        Color = [Color; PC_in(i).Color];
    end
    PC_out = pointCloud(Location, 'Color', Color);
end
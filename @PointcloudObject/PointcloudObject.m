classdef PointcloudObject < handle
    properties
        % Model from depthmap
        D_model
        size_model_x
        size_model_y
        K_model
        % Data from depthmap
        D_data
        size_data_x
        size_data_y
        K_data
        % Type
        agent
        % Pointclouds
        model
        data
        % Transformation estimates
        R
        S
        T
        residual
    end
    methods
        function obj = PointcloudObject(agent)
            obj.agent = agent;
        end
        get_depthmaps(obj, D1, D2)
        fg_association(obj, tform, n_points)
        bg_association(obj, tform, n_points)
        transformation_computation(obj)
    end
    methods (Static)
        pointcloud = dep2pcl(dep, img, calib, scale, offset)
        alpha = alpha_residuals(P_fg, P_bg)
        alpha = alpha_gss(P_fg, P_bg)
        tform = average_consensus(P_fg, P_bg, alpha);
        Pout = apply_T(Pin, R, S, T)
        T_matrix = RST2t_matrix(tform)
        pc_tform = t_matrix2pc_tform(tform)
        PC_out = combine_pointclouds(PC_in)
        plot_registration_result(P_fg, P_bg, T)
        plot_registration_result2D(P_fg, P_bg, T)
        show_registration_result(P_fg, P_bg, T)
    end
end
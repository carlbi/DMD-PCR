function plot_registration_result(P_fg, P_bg, T)

n_frames = unpack(config(), "n_frames");

player = pcplayer([-5000 5000],[-5000 5000],[-5000 5000]);
for j = 1:n_frames
    if ~isOpen(player)
        break
    end
    PC1 = PointcloudObject.dep2pcl(...
        reshape(P_bg.D_model, [P_bg.size_model_y, P_bg.size_model_x]), ...
        "cyan", P_bg.K_model);
    PC2 = pctransform(PointcloudObject.dep2pcl(...
        reshape(P_bg.D_data, [P_bg.size_data_y, P_bg.size_data_x]), ...
        "magenta", P_bg.K_data), T.pc_tform); 
    PC3 = PointcloudObject.dep2pcl(...
        reshape(P_fg.D_model(:,j), [P_fg.size_model_y, P_fg.size_model_x]), ...
        "blue", P_fg.K_model);
    PC4 = pctransform(PointcloudObject.dep2pcl(...
        reshape(P_fg.D_data(:,j), [P_fg.size_data_y, P_fg.size_data_x]), ...
        "red", P_fg.K_data), T.pc_tform); 
    PC_combined = PointcloudObject.combine_pointclouds([PC1, PC2, PC3, PC4]);
    view(player,PC_combined);   
    pause(0.05)
end

end
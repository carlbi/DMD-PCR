function transformation_computation(obj)

P_data = obj.data;
P_model = obj.model;

com_data = mean(P_data, 2);
com_model = mean(P_model, 2);
C_data = P_data-com_data;
C_model = P_model-com_model;

% 2.1 Rotation
K = C_model*C_data.';
[U,~,V] = svd(K);
R = U*V.';
if det(R) == -1; disp('det(R) = -1'); end

% 2.2 Scaling
CR_data = R*C_data;
S = sum(C_model.*CR_data, 'all') / sum(CR_data.*CR_data, 'all');

% 2.3 Translation
T = com_model - S*R*com_data;

% Store
obj.R = R;
obj.S = S;
obj.T = T;

% Residual
obj.residual = rmse(obj.model,...
    PointcloudObject.apply_T(obj.data, R, S, T));

end
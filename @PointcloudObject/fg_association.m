function fg_association(obj, ICP, n_points)

if nargin < 3
    n_points = unpack(config(), "n_points");
end

n_frames = unpack(config(), "n_frames");
R = ICP.R;
S = ICP.S;
T = ICP.T;

% Compute foreground
P_data = [];
P_model = [];
for j = 1:n_frames
    PC1_fg = obj.dep2pcl(reshape(obj.D_model(:,j), [obj.size_model_y, obj.size_model_x]), ...
    "green", obj.K_model);
    PC2_fg = obj.dep2pcl(reshape(obj.D_data(:,j), [obj.size_data_y, obj.size_data_x]), ...
    "blue", obj.K_data);

    % Sampling
    no_samples = min([size(PC1_fg.Location, 1), ...
        size(PC2_fg.Location, 1), round(n_points/n_frames)]);
    PC1_idx = randperm(size(PC1_fg.Location, 1));
    PC2_idx = randperm(size(PC2_fg.Location, 1));
    PC1_samples = PC1_fg.Location(PC1_idx(1:no_samples),:);
    PC2_samples = PC2_fg.Location(PC2_idx(1:no_samples),:);
    
    % Matching
    Idx = knnsearch(obj.apply_T(PC1_samples, R, S, T), PC2_samples);
    P_data = [P_data PC1_samples(Idx,:).'];
    P_model = [P_model PC2_samples.'];
end

obj.data = P_data;
obj.model = P_model;

end
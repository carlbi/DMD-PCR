function Pout = apply_T(Pin, R, S, T)
    if size(Pin,1) == 3
        Pout = S*(R*Pin) + T;
    elseif size(Pin,2) == 3
        Pout = S*(Pin*R') + T';
    else
        error('Cannot determine dimensions of point set')
    end
end
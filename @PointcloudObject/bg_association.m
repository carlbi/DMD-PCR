function bg_association(obj, ICP, n_points)

if nargin < 3
    n_points = unpack(config(), "n_points");
end

R = ICP.R;
S = ICP.S;
T = ICP.T;

% Compute background
PC1_bg = obj.dep2pcl(reshape(obj.D_model, [obj.size_model_y, obj.size_model_x]), ...
    "green", obj.K_model);
PC2_bg = obj.dep2pcl(reshape(obj.D_data, [obj.size_data_y, obj.size_data_x]), ...
    "blue", obj.K_data);

% Sampling
no_samples = min([size(PC1_bg.Location, 1), ...
    size(PC2_bg.Location, 1), n_points]);
PC1_idx = randperm(size(PC1_bg.Location, 1));
PC2_idx = randperm(size(PC2_bg.Location, 1));
PC1_samples = PC1_bg.Location(PC1_idx(1:no_samples),:);
PC2_samples = PC2_bg.Location(PC2_idx(1:no_samples),:);

% Matching
Idx = knnsearch(obj.apply_T(PC1_samples, R, S, T), PC2_samples);
obj.data = PC1_samples(Idx,:).';
obj.model = PC2_samples.';

end
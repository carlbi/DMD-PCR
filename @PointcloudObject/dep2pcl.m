function pointcloud = dep2pcl(dep, img, calib, scale, offset)

if nargin == 3
    scale = 50000; % initial scale factor from stereolabs
    offset = 0;
elseif nargin == 4
    offset = 0;
elseif nargin == 5
    dep = 1-im2double(dep); % invert depth encoding
else
    error('Missing values') 
end


if isstring(img)
    switch img
        case 'red'
            rgb_color = zeros(3,3); rgb_color(1,1) = 1;
        case 'green'
            rgb_color = zeros(3,3); rgb_color(2,2) = 1;
        case 'blue'
            rgb_color = zeros(3,3); rgb_color(3,3) = 1;
        case 'yellow'
            rgb_color = eye(3,3); rgb_color(3,3) = 0;
        case 'magenta'
            rgb_color = eye(3,3); rgb_color(2,2) = 0;
        case 'cyan'
            rgb_color = eye(3,3); rgb_color(1,1) = 0;
        otherwise
            error('Color not red, green or blue')
    end
    img = ones(size(dep));
else
    rgb_color = eye(3);
end

num_row = size(dep,1); % rows -> v
num_col = size(dep,2); % cols -> u

fx = calib(1);
fy = calib(2);
cx = calib(3); % x <-> u
cy = calib(4); % y <-> v

% disp('Calculating Pointcloud...')

% Fast version - TUM
% tic
[V,U] = ind2sub([num_row,num_col],1:num_row*num_col);
Z = dep(:)*scale + offset;
X = ((U' - cx)/ fx) .* Z;
Y = ((V' - cy)/ fy) .* Z;
PC = [X Y Z];
% Color = reshape(img,[num_row*num_col,3]);
Color = repmat(img(:),[1, 3])*rgb_color;
% toc

% % Slow version - TUM
% PC = zeros(num_col*num_row,3);
% Color = zeros(num_col*num_row,3);
% %Indices = zeros(num_x*num_y,2);
% tic
% for u = 1:num_col
%     for v = 1:num_row
%             if isnan(dep(v,u))
%                 continue
%             end
%             Z = dep(v,u)*scale+offset; % reverse because v=col, u=row
%             color = img(v,u,:); % reverse because v=col, u=row
%             X = (u - cx) * Z / fx;
%             Y = (v - cy) * Z / fy;
%             PC((u-1)*num_col + v,:) = [X Y Z];
%             Color((u-1)*num_col + v,:) = [color(1) color(2) color(3)];
%             % Indices((u-1)*num_y + v,:) = [u v];
%     end
% end
% toc

% Remove zeros rows
Color( any(~PC,2), : ) = [];
PC( any(~PC,2), : ) = [];

pointcloud = pointCloud(PC,...
    'Color',Color);
%     'Zlimits', [offset; scale+offset],...
%     'XLimits', [-((num_col/2-cx)/fx*(scale+offset)) ((num_col/2-cx)/fx)*(scale+offset)],...
%     'YLimits', [-((num_col/2-cy)/fy*(scale+offset)) ((num_col/2-cy)/fy)*(scale+offset)]);
end


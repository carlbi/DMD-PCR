function figure_2_2_3_3()

x = linspace(-5, 5, 1001);
alpha = logistic(x);

figure
plot(x, alpha)
ylabel('\alpha_k')
xlabel('\Delta R_k')
title('The logistic function')

end
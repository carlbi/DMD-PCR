function figure_3_3()

D = DepthmapObject;
D.load_view(2, "IBI");

[X_lims, Y_lims, Z_lims] = unpack(config(), ...
    ["X_lims", "Y_lims", "Z_lims"]);

show_frame = 60;

pc = PointcloudObject.dep2pcl(reshape(D.X(:,show_frame), [D.size_y, D.size_x]), ...
        reshape(D.RGB(:,show_frame), [D.size_y, D.size_x]), D.K);
fg = pc.Location(:,1) > X_lims(1) & pc.Location(:,1) < X_lims(2) & ...
         pc.Location(:,2) > Y_lims(1) & pc.Location(:,2) < Y_lims(2) & ...
         pc.Location(:,3) > Z_lims(1) & pc.Location(:,3) < Z_lims(2);
pc.Color(fg, 2) = 0; pc.Color(fg, 3) = 0;

figure
for v = 1:4
    subplot(2,2,v)
    hold on
    pcshow(pc)
    X = [X_lims(1);X_lims(2);X_lims(2);X_lims(1);X_lims(1)];
    Y = [Y_lims(1);Y_lims(1);Y_lims(2);Y_lims(2);Y_lims(1)];
    Z = [Z_lims(1);Z_lims(1);Z_lims(1);Z_lims(1);Z_lims(1)];
    plot3(X,Y,Z, 'r-');   % draw a square in the xy plane
    plot3(X,Y,Z+(Z_lims(2) - Z_lims(1)), 'r-'); % draw a second square
    for k=1:length(X)-1
        plot3([X(k);X(k)],[Y(k);Y(k)],[Z_lims(1);Z_lims(2)], 'r-');
    end
    switch v
        case 1
            view([0 -0.1 -1])
        case 2
            view([0 -0.3 -1])
        case 3
            view([0.5 -0.1 -1])
            camroll(-90)
        case 4
            view([-0.5 -0.1 -1])
            camroll(90)
    end
    set(gca, 'XColor', [0.5 0.5 0.5], 'YColor', [0.5 0.5 0.5], 'ZColor', [0.5 0.5 0.5])
end

figure
imshow(reshape(D.RGB(:,show_frame), [D.size_y, D.size_x]))

end
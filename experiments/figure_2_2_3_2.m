function figure_2_2_3_2()

D = DepthmapObject;
D.load_view(1, "IBI");

Dict = DictionaryObject;
Dict.get_training_data(D);
Dict.train_dictionary();

D.Y = Dict.compute_coefficients(D, "X");

figure
% Run dmd for custom r
dt = unpack(config(), "dt");
r = 25;
% on images
[~, lambdas] = DepthmapObject.dmd(D.X, r);
omegas = log(lambdas)/dt;
subplot(1,2,1)
plot(omegas, 'o')
title('Continuous time DMD eigenvalues on images')
% on betas
[~, lambdas] = DepthmapObject.dmd(D.Y, r);
omegas = log(lambdas)/dt;
subplot(1,2,2)
plot(omegas, 'o')
title('Continuous time DMD eigenvalues on coefficients')

end
function figure_3_2_2()

lambdas = logspace(-9, 0, 10);
psnr = zeros(10, 2);
mse = zeros(10,2);

for l = progress(1:numel(lambdas))
    % DL
    S = load(sprintf('experiments/figure_3_2_1/DL_lambda_%d.mat', l));
    [mse(l,1), psnr(l,1)] = mse_psnr(S.Depthmap, S.Reconstructed);
    % nsDL
    S = load(sprintf('experiments/figure_3_2_1/nsDL_lambda_%d.mat', l));
    [mse(l,2), psnr(l,2)] = mse_psnr(S.Depthmap, S.Reconstructed);
end

figure
dl = subplot(1,2,1);
semilogx(lambdas,psnr(:,1), '-o')
ylabel('PSNR (dB)')
xlabel('\lambda_{\beta}')
title('DL')
nsdl = subplot(1,2,2);
semilogx(lambdas, psnr(:,2), '-o')
xlabel('\lambda_{\beta}')
title('nsDL')
linkaxes([dl,nsdl],'y')



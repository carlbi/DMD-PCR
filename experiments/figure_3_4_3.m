function figure_3_4_3()

D1 = DepthmapObject;
D2 = DepthmapObject;

D1.load_view(1, 'MC');
D2.load_view(2, 'MC');

D1.run_dmd()
D2.run_dmd()

D1.separate_fg_bg()
D2.separate_fg_bg()

D1.generate_fg_mask(false, "t-and", 1);
D2.generate_fg_mask(false, "t-and", 1);
 
% Registration

P_fg = PointcloudObject("Foreground");
P_bg = PointcloudObject("Background");

P_fg.get_depthmaps(D1, D2);
P_bg.get_depthmaps(D1, D2);

ICP = ICPObject;
ICP.initialize(P_fg);
% Initial error
P_fg.fg_association(ICP);
P_bg.bg_association(ICP);
ICP.store_history(P_fg, P_bg);

n_iter_icp = unpack(config(), "n_iter_icp");

for i = progress(1:n_iter_icp)
    % 1. Point association via knn-search
    P_fg.fg_association(ICP);
    P_bg.bg_association(ICP);

    % 2. Transformation computation via svd
    P_fg.transformation_computation();
    P_bg.transformation_computation();
    
    % 3. Consensus
    ICP.alpha_residuals(P_fg, P_bg);
    % ICP.alpha_gss(P_fg, P_bg);
    ICP.average_consensus(P_fg, P_bg);
    
    % Store History
    ICP.store_history(P_fg, P_bg);

    if ICP.converged(); ICP.iter_conv = i; break; end
end
disp('ICP converged or max. iterations reached!');

ICP.create_transforms();
ICP.final_registration_error(P_fg, P_bg);

show_frame = 60;

PC1 = PointcloudObject.dep2pcl(reshape(D1.X(:,show_frame), [D1.size_y, D1.size_x]), ...
    reshape(D1.RGB(:,show_frame), [D1.size_y, D1.size_x]), D1.K);
PC2 = PointcloudObject.dep2pcl(reshape(D2.X(:,show_frame), [D2.size_y, D2.size_x]), ...
    reshape(D2.RGB(:,show_frame), [D2.size_y, D2.size_x]), D2.K);

PC1s = pcdownsample(PC1, 'gridAverage', 100);
PC2s = pcdownsample(PC2, 'gridAverage', 100);

[tform_icp, PC2_icp, rmse_icp] = pcregistericp(PC2s, PC1s);
[tform_cpd, PC2_cpd, rmse_cpd] = pcregistercpd(PC2s, PC1s, 'Transform', 'Rigid');
[tform_ndt, PC2_ndt, rmse_ndt] = pcregisterndt(PC2s, PC1s, 500);

PC1.Color(:, 1) = 0; PC1.Color(:, 2) = 0;
PC2.Color(:, 1) = 0; PC2.Color(:, 3) = 0;
PC2_icp.Color(:, 1) = 0; PC2_icp.Color(:, 3) = 0;
PC2_cpd.Color(:, 1) = 0; PC2_cpd.Color(:, 3) = 0;
PC2_ndt.Color(:, 1) = 0; PC2_ndt.Color(:, 3) = 0;

figure
% Initial
PC_combined = PointcloudObject.combine_pointclouds([PC1, pctransform(PC2, ICP.pc_tform)]);
subplot(2,2,1)
pcshow(PC_combined); 
view([0 -0.1 -1])
error_mine = 0.5*(ICP.final_bg_error + ICP.final_fg_error);
title(['\color{black}' sprintf('Proposed, rmse = %dmm', round(error_mine))])
set(gca, 'XColor', [1 1 1], 'YColor', [1 1 1], 'ZColor', [1 1 1])

% ICP
PC_combined = PointcloudObject.combine_pointclouds([PC1, pctransform(PC2, tform_icp)]);
subplot(2,2,2)
pcshow(PC_combined); 
view([0 -0.1 -1])
title(['\color{black}' sprintf('ICP, rmse = %dmm', round(rmse_icp))])
set(gca, 'XColor', [1 1 1], 'YColor', [1 1 1], 'ZColor', [1 1 1])

% CPD
PC_combined = PointcloudObject.combine_pointclouds([PC1, pctransform(PC2, tform_cpd)]);
subplot(2,2,3)
pcshow(PC_combined); 
view([0 -0.1 -1])
title(['\color{black}' sprintf('CPD, rmse = %dmm', round(rmse_cpd))])
set(gca, 'XColor', [1 1 1], 'YColor', [1 1 1], 'ZColor', [1 1 1])

% NDT
PC_combined = PointcloudObject.combine_pointclouds([PC1, pctransform(PC2, tform_ndt)]);
subplot(2,2,4)
pcshow(PC_combined); 
view([0 -0.1 -1])
title(['\color{black}' sprintf('NDT, rmse = %dmm', round(rmse_ndt))])
set(gca, 'XColor', [1 1 1], 'YColor', [1 1 1], 'ZColor', [1 1 1])

end


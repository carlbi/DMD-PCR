function figure_2_2_1_3()

D = DepthmapObject;
D.load_view(1);
frame_number = 85; % show some arbitrary frame for illustration purposes

RGB_show = reshape(D.RGB(:, frame_number), [D.size_y, D.size_x]);
X_show = reshape(D.X(:, frame_number), [D.size_y, D.size_x]);
PC_show = PointcloudObject.dep2pcl(X_show, RGB_show, D.K);

figure
subplot(1,3,1)
imshow(RGB_show)
subplot(1,3,2)
pcshow(PC_show)
subplot(1,3,3)
pcshow(PC_show)



end


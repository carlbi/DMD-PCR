function figure_3_3_2()

% Please comment in the correct dataset in config.m
% Also comment in the matching view and ground truth below

D = DepthmapObject;
% D.load_view(2, 'MC'); % view for large room dataset
D.load_view(1, 'MC'); % view for small room dataset

% GT = compute_gt_large(D);
GT = compute_gt_small();

% Compute coeff

Dict = DictionaryObject;
Dict.get_training_data(D);
Dict.train_dictionary();

D.Y = Dict.compute_coefficients(D, "X");

% Standard DMD
D.run_dmd(false)% dl=false
D.separate_fg_bg(false)

% DL DMD
D.run_dmd(true) % dl=true
D.separate_fg_bg(true)
D.reconstruct_from_coeff(Dict)

PRF = zeros(12, 3);
PRF_dl = zeros(12, 3);

scheme = ["bg-t", "fg-t", "t-and"];

% std 1
for n = 1:4
    for i = 1:3
        D.generate_fg_mask(true, scheme(i), n);
        D.generate_fg_mask(false, scheme(i), n);
        [PRF(3*(n-1)+i,1), PRF(3*(n-1)+i,2), PRF(3*(n-1)+i,3)] = pre_rec_f1(GT, D.Mx_fg);
        [PRF_dl(3*(n-1)+i,1), PRF_dl(3*(n-1)+i,2), PRF_dl(3*(n-1)+i,3)] = pre_rec_f1(GT, D.Mr_fg);
    end
end

figure
hold on
plot(PRF(:, 1), 'bo')
plot(PRF(:, 2), 'ro')
plot(PRF(:, 3), 'go')
plot(PRF_dl(:, 1), 'b*')
plot(PRF_dl(:, 2), 'r*')
plot(PRF_dl(:, 3), 'g*')
ylim([0 1])
xlim([0.5 12.5])
plot([3.5 3.5], [0 1], 'k-')
plot([6.5 6.5], [0 1], 'k-')
plot([9.5 9.5], [0 1], 'k-')
legend('pre', 'rec', 'f1', 'pre_{dl}', 'rec_{dl}', 'f1_{dl}', 'Location', 'south')
xlabel = ["BG-t_1", "FG-t_1", "t_1-AND", "BG-t_2", "FG-t_2", "t_2-AND", ...
    "BG-t_3", "FG-t_3", "t_3-AND", "BG-t_4", "FG-t_4", "t_4-AND"];
set(gca, 'Xtick', 1:12, 'XTickLabel',xlabel);
title('Precision, Recall and F1-Score')

end
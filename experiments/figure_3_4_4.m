function figure_3_4_4()

D1 = DepthmapObject;
D2 = DepthmapObject;

D1.load_view(1, 'MC');
D2.load_view(2, 'MC');

D1.run_dmd()
D2.run_dmd()

D1.separate_fg_bg()
D2.separate_fg_bg()

D1.generate_fg_mask();
D2.generate_fg_mask(); 
 
% Registration

P_fg = PointcloudObject("Foreground");
P_bg = PointcloudObject("Background");

P_fg.get_depthmaps(D1, D2);
P_bg.get_depthmaps(D1, D2);

n_trials = 100;
n_iter_icp = unpack(config(), "n_iter_icp");
rand_rot = (rand(3, n_trials)-0.5)*pi;
rand_scale = rand(1, n_trials)+0.5;
rand_trans = (rand(3, n_trials)-0.5)*2000;
Trials = zeros(9, n_trials);

for r = (1:n_trials)
    r
    ICP = ICPObject;
    ICP.initialize();
    % Set initials
    ICP.R = eul2rotm(rand_rot(:,r)');
    ICP.S = rand_scale(r);
    ICP.T = rand_trans(:,r);
    % Run ICP
    P_fg.fg_association(ICP);
    P_bg.bg_association(ICP);
    ICP.store_history(P_fg, P_bg);
    for i = 1:n_iter_icp
        % 1. Point association via knn-search
        P_fg.fg_association(ICP);
        P_bg.bg_association(ICP);
        % 2. Transformation computation via svd
        P_fg.transformation_computation();
        P_bg.transformation_computation();
        % 3. Consensus
        ICP.alpha_residuals(P_fg, P_bg);
        ICP.average_consensus(P_fg, P_bg);
        % History
        ICP.store_history(P_fg, P_bg);
        if ICP.converged(); ICP.iter_conv = i; break; end
    end
    disp('ICP converged or max. iterations reached!');
    ICP.final_registration_error(P_fg, P_bg);
    Trials(:,r) = [180*rotm2eul(ICP.R)'; ICP.S; ICP.T; 
                   ICP.final_bg_psnr; ICP.final_fg_psnr];
end

% Plot
trial_fields = ["\gamma_x", "\gamma_y", "\gamma_z", ...
                "s", "t_x", "t_y", ...
                "t_z", "psnr_{bg}", "psnr_{fg}"];
x_labels = ["deg", "deg", "deg", "", "mm", "mm", "mm", "dB", "dB"];

figure
for j = 1:9
    subplot(3,3,j)
    histogram(Trials(j,:))
    xlabel(x_labels(j))
    ylim([0 100])
    title(trial_fields(j))
end


end
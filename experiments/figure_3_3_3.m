function figure_3_3_3()

D = DepthmapObject;
D.load_view(2, 'MC'); % view for large room dataset
X_true = D.X;


Dict = DictionaryObject;
Dict.get_training_data(D);
Dict.train_dictionary();

GT = compute_gt_large(D);

r_noise = [0.01, 0.05, 0.1, 0.2, 0.3]; % noise ratio

PRF = zeros(5, 3);
PRF_dl = zeros(5, 3);

for n = 1:numel(r_noise)
    % Add random noise
    n_noise = round(r_noise(n)*numel(X_true));
    X_corrupted = X_true;
    x_noise = normrnd(0, std(X_true(:)), n_noise, 1);
    loc_noise = ceil(rand(n_noise, 1)*numel(X_true));
    X_corrupted(loc_noise) = X_corrupted(loc_noise) + x_noise;
    D.X = X_corrupted;
    % DL
%     Dict = DictionaryObject;
%     Dict.get_training_data(D);
%     Dict.train_dictionary();
    D.Y = Dict.compute_coefficients(D, "X");
    % Standard DMD
    D.run_dmd(false)% dl=false
    D.separate_fg_bg(false)
    % DL DMD
    D.run_dmd(true) % dl=true
    D.separate_fg_bg(true)
    D.reconstruct_from_coeff(Dict)
    % Get masks
    D.generate_fg_mask(true, "bg-t");
    D.generate_fg_mask(false, "bg-t");
    [PRF(n,1), PRF(n,2), PRF(n,3)] = pre_rec_f1(GT, D.Mx_fg);
    [PRF_dl(n,1), PRF_dl(n,2), PRF_dl(n,3)] = pre_rec_f1(GT, D.Mr_fg);
end

figure
hold on
plot(r_noise, PRF(:, 1), 'bo-')
plot(r_noise, PRF(:, 2), 'ro-')
plot(r_noise, PRF(:, 3), 'go-')
plot(r_noise, PRF_dl(:, 1), 'b*-')
plot(r_noise, PRF_dl(:, 2), 'r*-')
plot(r_noise, PRF_dl(:, 3), 'g*-')
ylim([0 1])
xlabel('Ratio of noisy pixels')
legend('pre', 'rec', 'f1', 'pre_{dl}', 'rec_{dl}', 'f1_{dl}', 'Location', 'south')
title('Performance in the presence of noise')

end
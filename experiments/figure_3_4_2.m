function figure_3_4_2()

D1 = DepthmapObject;
D2 = DepthmapObject;

D1.load_view(1, 'MC');
D2.load_view(2, 'MC');

D1.run_dmd()
D2.run_dmd()

D1.separate_fg_bg()
D2.separate_fg_bg()

D1.generate_fg_mask();
D2.generate_fg_mask(); 

% Registration

P_fg = PointcloudObject("Foreground");
P_bg = PointcloudObject("Background");

P_fg.get_depthmaps(D1, D2);
P_bg.get_depthmaps(D1, D2);

ICP = ICPObject;
ICP.initialize(P_fg);
% Initial error
P_fg.fg_association(ICP);
P_bg.bg_association(ICP);
ICP.store_history(P_fg, P_bg);

n_iter_icp = unpack(config(), "n_iter_icp");

for i = progress(1:n_iter_icp)
    % 1. Point association via knn-search
    P_fg.fg_association(ICP);
    P_bg.bg_association(ICP);

    % 2. Transformation computation via svd
    P_fg.transformation_computation();
    P_bg.transformation_computation();
    
    % 3. Consensus
    ICP.alpha_residuals(P_fg, P_bg);
    % ICP.alpha_gss(P_fg, P_bg);
    ICP.average_consensus(P_fg, P_bg);
    
    % Store History
    ICP.store_history(P_fg, P_bg);

    if ICP.converged(); ICP.iter_conv = i; break; end
end
disp('ICP converged or max. iterations reached!');
ICP.create_transforms();
ICP.final_registration_error(P_fg, P_bg);

%
ICP2 = ICPObject;
ICP2.initialize(P_fg);
% Initial error
P_fg.fg_association(ICP2);
P_bg.bg_association(ICP2);
ICP2.store_history(P_fg, P_bg);

n_iter_icp = unpack(config(), "n_iter_icp");

for i = progress(1:n_iter_icp)
    % 1. Point association via knn-search
    P_fg.fg_association(ICP2);
    P_bg.bg_association(ICP2);

    % 2. Transformation computation via svd
    P_fg.transformation_computation();
    P_bg.transformation_computation();
    
    % 3. Consensus
    % ICP.alpha_residuals(P_fg, P_bg);
    ICP2.alpha_gss(P_fg, P_bg);
    ICP2.average_consensus(P_fg, P_bg);
    
    % Store History
    ICP2.store_history(P_fg, P_bg);

    if ICP2.converged(); ICP2.iter_conv = i; break; end
end
disp('ICP converged or max. iterations reached!');
ICP2.create_transforms();
ICP2.final_registration_error(P_fg, P_bg);


figure('Renderer', 'painters', 'Position', [400 400 300 200])
hold on
plot(1:2, [ICP.final_fg_psnr ICP.final_bg_psnr], 'bo--')
plot(1:2, [ICP2.final_fg_psnr ICP2.final_bg_psnr], 'ro--')
xlim([0.5 2.5])
legend('balanced', 'optimal')
xlabel = ["PSNR-fg", "PSNR-bg"];
set(gca, 'Xtick', 1:4, 'XTickLabel',xlabel);
title('PSNR, balanced vs. optimal')

 end


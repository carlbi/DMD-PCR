function figure_2_2_3_4()

D = DepthmapObject;
D.load_view(1, 'MC');

[dt, t, tol] = unpack(config(), ["dt", "t", "tol"]);
r = 195;

% Run dmd on depthmaps
D.run_dmd()
omegas = log(D.DMD_eigenvalues)/dt;
Phi = D.DMD_Modes;
b = Phi \ D.X(:, 1); % amplitude
bg = find(abs(omegas)<tol); % Threshold for background separation
fg = setdiff(1:r, bg);
omega_fg = omegas(fg); % foreground
Phi_fg = Phi(:,fg); % DMD foreground modes
omega_bg = omegas(bg); % background
% Correct
b_fg = b(fg);
X_dep_fg_correct = zeros(numel(omega_fg), length(t));
for tt = 1:length(t)
    X_dep_fg_correct(:, tt) = b_fg .* exp(omega_fg .* t(tt));
end
X_dep_fg_correct = real(Phi_fg * X_dep_fg_correct);
X_dep_fg_correct = X_dep_fg_correct(1:end, :);
% Wrong
b_fg = Phi_fg \ D.X(:, 1); % amplitude
X_dep_fg_wrong = zeros(numel(omega_fg), length(t));
for tt = 1:length(t)
    X_dep_fg_wrong(:, tt) = b_fg .* exp(omega_fg .* t(tt));
end
X_dep_fg_wrong = real(Phi_fg * X_dep_fg_wrong);
X_dep_fg_wrong = X_dep_fg_wrong(1:end, :);

X_dep = D.X;

% Run dmd on rgb
D.X = D.RGB;
D.run_dmd()

omegas = log(D.DMD_eigenvalues)/dt;
Phi = D.DMD_Modes;
b = Phi \ D.X(:, 1); % amplitude
bg = find(abs(omegas)<tol); % Threshold for background separation
fg = setdiff(1:r, bg);
omega_fg = omegas(fg); % foreground
Phi_fg = Phi(:,fg); % DMD foreground modes
% Correct
b_fg = b(fg);
X_rgb_fg_correct = zeros(numel(omega_fg), length(t));
for tt = 1:length(t)
    X_rgb_fg_correct(:, tt) = b_fg .* exp(omega_fg .* t(tt));
end
X_rgb_fg_correct = real(Phi_fg * X_rgb_fg_correct);
X_rgb_fg_correct = X_rgb_fg_correct(1:end, :);
% Wrong
b_fg = Phi_fg \ D.X(:, 1); % amplitude
X_rgb_fg_wrong = zeros(numel(omega_fg), length(t));
for tt = 1:length(t)
    X_rgb_fg_wrong(:, tt) = b_fg .* exp(omega_fg .* t(tt));
end
X_rgb_fg_wrong = real(Phi_fg * X_rgb_fg_wrong);
X_rgb_fg_wrong = X_rgb_fg_wrong(1:end, :);

X_rgb = D.X;


% Plot

figure
for i = 1:4
    subplot(4,4,i)
    imshow(10*reshape(X_dep(:,20*i), [D.size_y, D.size_x]))
    title('Depthmap')
    subplot(4,4,i+4)
    imshow(40*reshape(-X_dep_fg_wrong(:,20*i), [D.size_y, D.size_x]))
    title('- wrong')
    subplot(4,4,i+8)
    imshow(20*reshape(X_dep_fg_correct(:,20*i), [D.size_y, D.size_x]))
    title('+ foreground')
    subplot(4,4,i+12)
    imshow(40*reshape(-X_dep_fg_correct(:,20*i), [D.size_y, D.size_x]))
    title('- foreground')
end

figure
for i = 1:4
    subplot(4,4,i)
    imshow(reshape(X_rgb(:,20*i), [D.size_y, D.size_x]))
    title('RGB')
    subplot(4,4,i+4)
    imshow(10*reshape(-X_rgb_fg_wrong(:,20*i), [D.size_y, D.size_x]))
    title('- wrong')
    subplot(4,4,i+8)
    imshow(10*reshape(X_rgb_fg_correct(:,20*i), [D.size_y, D.size_x]))
    title('+ foreground')
    subplot(4,4,i+12)
    imshow(10*reshape(-X_rgb_fg_correct(:,20*i), [D.size_y, D.size_x]))
    title('- foreground')
end

end
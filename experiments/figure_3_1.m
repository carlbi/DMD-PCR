function figure_3_1()

D1 = DepthmapObject;
D1.load_view(1);
D2 = DepthmapObject;
D2.load_view(2);

% Plots
show_frame = 60;

figure
imshowpair(reshape(D1.RGB(:,show_frame), [D1.size_y, D1.size_x]), ...
reshape(D2.RGB(:,show_frame), [D2.size_y, D2.size_x]), 'montage')
figure
imshowpair(10*reshape(D1.X(:,show_frame), [D1.size_y, D1.size_x]), ...
10*reshape(D2.X(:,show_frame), [D2.size_y, D2.size_x]), 'montage')

end
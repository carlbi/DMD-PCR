clearvars; close all; clc
addpath('utils')
addpath('utils/TFOCS-master')

D = DepthmapObject;
D.load_view(1);

[p_size, n_atoms] = unpack(config(), ["p_size", "n_atoms"]);
lambdas = logspace(-9, 0, 10);
Save_Dictionary = zeros(n_atoms*p_size^2, numel(lambdas));
Save_Reconstructed = zeros(numel(D.X), numel(lambdas));

parfor i = 1:numel(lambdas)
    Dict = DictionaryObject;
    Dict.lambda_DL = lambdas(i);
    Dict.get_training_data(D);
    Dict.train_dictionary_ns();
    % Temporary DepthmapObject
    D_temp = DepthmapObject;
    D_temp.size_y = D.size_y;
    D_temp.size_x = D.size_x;
    D_temp.Y = Dict.compute_coefficients(D, "X");
    D_temp.R = Dict.reconstruct_frames(D_temp, "Y")
    % Save file
    Save_Dictionary(:,i) = Dict.Dict(:);
    Save_Reconstructed(:,i) = D_temp.R(:);
end

for i= 1:numel(lambdas)
   Dictionary = reshape(Save_Dictionary(:,i), [n_atoms, p_size^2]);
   Depthmap = D.X;
   Reconstructed = reshape(Save_Reconstructed(:,i), size(D.X));
   filename = sprintf('nsDL_lambda_%d', i);
   save(filename, 'Dictionary', 'Depthmap', 'Reconstructed')
end



function figure_2_2_3_3()

D = DepthmapObject;
D.load_view(1, "IBI");

% Run dmd for custom r
[dt, tol] = unpack(config(), ["dt", "tol"]);
r = 25;
[Phi, lambdas] = DepthmapObject.dmd(D.X, r);
omegas = log(lambdas)/dt;
bg = find(abs(omegas)<tol);
Phi_bg = Phi(:,bg);
Phi_bg(Phi_bg<0) = 0; % delete negative values

frame_number = 20; % arbitrary frame for illustration purposes

figure
subplot(1,3,1)
imshow(reshape(D.RGB(:,frame_number), [D.size_y D.size_x]))
subplot(1,3,2)
imshow(rescale(reshape(D.X(:,frame_number), [D.size_y D.size_x])))
subplot(1,3,3)
imshow(rescale(real(reshape(Phi_bg, [D.size_y D.size_x]))))

end
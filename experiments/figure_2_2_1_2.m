function figure_2_2_1_2()

D = DepthmapObject;
D.load_view(2);
frame_number = 30; % show some arbitrary frame for illustration purposes

RGB_show = reshape(D.RGB(:, frame_number), [D.size_y, D.size_x]);
X_show = 10*reshape(D.X(:, frame_number), [D.size_y, D.size_x]);
X_show(X_show==0) = 1;

figure
subplot(1,2,1)
imshow(RGB_show)
subplot(1,2,2)
imshow(X_show)


end


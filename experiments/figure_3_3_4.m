function figure_3_3_4()

D = DepthmapObject;
D.load_view(2, 'MC'); % view for large room dataset
% scheme = "fg-t";
scheme = "bg-t";

GT = compute_gt_large(D);

Dict = DictionaryObject;
Dict.get_training_data(D);
Dict.train_dictionary();
D.Y = Dict.compute_coefficients(D, "X");

r_dmd = [195, 120, 60, 30, 15, 10, 5, 3]; % noise ratio

PRF = zeros(numel(r_dmd), 3);
PRF_dl = zeros(numel(r_dmd), 3);
PSNR = zeros(numel(r_dmd), 1);
PSNR_dl = zeros(numel(r_dmd), 1);

for n = 1:numel(r_dmd)
    D.DMD_truncation = r_dmd(n);
    % Standard DMD
    D.run_dmd(false)% dl=false
    D.separate_fg_bg(false)
    % DL DMD
    D.run_dmd(true) % dl=true
    D.separate_fg_bg(true)
    D.reconstruct_from_coeff(Dict)
    % Masking
    D.generate_fg_mask(true, scheme);
    D.generate_fg_mask(false, scheme);
    [PRF(n,1), PRF(n,2), PRF(n,3)] = pre_rec_f1(GT, D.Mx_fg);
    [PRF_dl(n,1), PRF_dl(n,2), PRF_dl(n,3)] = pre_rec_f1(GT, D.Mr_fg);
    % Evaluating reconstruction
    [~, PSNR(n)] = mse_psnr(D.X, D.X_dmd);
    [~ , PSNR_dl(n)] = mse_psnr(D.R, D.R_dmd);
end

figure
hold on
plot(r_dmd, PRF(:, 1), 'bo-')
plot(r_dmd, PRF(:, 2), 'ro-')
plot(r_dmd, PRF(:, 3), 'go-')
plot(r_dmd, PRF_dl(:, 1), 'b*-')
plot(r_dmd, PRF_dl(:, 2), 'r*-')
plot(r_dmd, PRF_dl(:, 3), 'g*-')
ylim([0 1])
xlabel("Truncation of DMD modes (max. 200 frames)")
legend('pre', 'rec', 'f1', 'pre_{dl}', 'rec_{dl}', 'f1_{dl}', 'Location', 'south')
title('Performance of separation under truncation')

figure
hold on
plot(r_dmd, PSNR, 'ko-')
plot(r_dmd, PSNR_dl, 'k*-')
legend('PSNR', 'PSNR_{dl}')
xlabel("Truncation of DMD modes (max. 200 frames)")
ylabel("PSNR (dB)")
title('Reconstruction quality under truncation')

end
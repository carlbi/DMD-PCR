clearvars; close all; clc
addpath('utils')
addpath('utils/TFOCS-master')

%% Load data

conf = config();

D1 = DepthmapObject;
D2 = DepthmapObject;

D1.load_view(1, 'MC');
D2.load_view(2, 'MC');

%% Dictionary Learning

Dict = DictionaryObject;
Dict.get_training_data(D1); % use D1 for training
Dict.train_dictionary();

D1.Y = Dict.compute_coefficients(D1, "X");
D2.Y = Dict.compute_coefficients(D2, "X");

%% Compute DMD modes

D1.run_dmd()
D2.run_dmd()

%% Stabilize 

% D1.stabilize_modes();
% D2.stabilize_modes();

%% Separate foreground and background

D1.separate_fg_bg()
D2.separate_fg_bg()

%% Reconstruct depthmaps (only if DL)

D1.reconstruct_from_coeff(Dict)
D2.reconstruct_from_coeff(Dict)

%% Generate foreground mask and get depth values

D1.generate_fg_mask();
D2.generate_fg_mask();

%% Create point cloud objects

P_fg = PointcloudObject("Foreground");
P_bg = PointcloudObject("Background");

P_fg.get_depthmaps(D1, D2);
P_bg.get_depthmaps(D1, D2);

%% ICP

ICP = ICPObject;
ICP.initialize(P_fg);
% Initial error
P_fg.fg_association(ICP);
P_bg.bg_association(ICP);
ICP.store_history(P_fg, P_bg);

for i = progress(1:conf.n_iter_icp)
    % 1. Point association via knn-search
    P_fg.fg_association(ICP);
    P_bg.bg_association(ICP);

    % 2. Transformation computation via svd
    P_fg.transformation_computation();
    P_bg.transformation_computation();
    
    % 3. Consensus
    %ICP.alpha_residuals(P_fg, P_bg);
    ICP.alpha_gss(P_fg, P_bg);
    ICP.average_consensus(P_fg, P_bg);
    
    % Store History
    ICP.store_history(P_fg, P_bg);

    if ICP.converged(); ICP.iter_conv = i; break; end
end
disp('ICP converged or max. iterations reached!');

ICP.create_transforms();
ICP.final_registration_error(P_fg, P_bg);

%% ICP history

ICP.plot_history();

%% Show result

PointcloudObject.plot_registration_result(P_fg, P_bg, ICP)
% PointcloudObject.plot_registration_result2D(P_fg, P_bg, ICP)


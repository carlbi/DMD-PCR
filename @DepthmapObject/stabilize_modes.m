function stabilize_modes(obj)

dt = unpack(config(), "dt");

Modes = obj.DMD_Modes;
lambdas = obj.DMD_eigenvalues;
omegas = log(lambdas)/dt;

% Find unstable modes
unstable = find(real(omegas)>0);
omegas(unstable) = imag(unstable);

lambdas_s = exp(dt*omegas);

[Y_dmd, ~] = obj.reconstruct_dmd(Modes, lambdas, obj.Y); 

% W matrix
[theta, Q] = cart2pol(real(Modes), imag(Modes));

% V matrix
[phi, r] = cart2pol(real(lambdas), imag(lambdas));
[phi_s, r_s] = cart2pol(real(lambdas_s), imag(lambdas_s));


[D_bef,~] = computeDWV(Y_dmd, Q, theta, r, phi);
fprintf('Error after initial separation: %d \n', norm(D_bef))

[D_aft,~] = computeDWV(Y_dmd, Q, theta, r_s, phi_s);
fprintf('Error after zero-setting positive real parts: %d \n', norm(D_aft))

% Run block-wise optimization
[Q_out, ~,~] = solver_NNS('Q', Y_dmd, Q, theta, r_s, phi_s);
[theta_out, ~,~] = solver_NNS('Theta', Y_dmd, Q_out, theta, r_s, phi_s);

[D_opt,~,~] = computeDWV(Y_dmd, Q_out, theta, r_s, phi_s);
fprintf('Error after optimization: %d \n', norm(D_opt))

% Reconstruct
[Phi_nns_re, Phi_nns_im] = pol2cart(theta_out, Q_out);
[lambda_nns_re, lambda_nns_im] = pol2cart(phi_s, r_s);

obj.DMD_Modes = Phi_nns_re + 1i*Phi_nns_im;
obj.DMD_eigenvalues = lambda_nns_re + 1i*lambda_nns_im;

end
function [X_dmd, X_dmd_ls_fit] = reconstruct_dmd(Phi, lambda, X)

[dt, t] = unpack(config(), ["dt", "t"]);

b = Phi\X(:,1);
omega = log(lambda)/dt;

m = size(X, 2);
time_dynamics = zeros(size(Phi, 2), m);
for iter = 1:m
    time_dynamics(:,iter) = lambda.^(iter-1);
end
X_dmd = Phi * time_dynamics;

for iter = 1:m
    time_dynamics(:,iter) = b.*lambda.^(iter-1);
end
X_dmd_ls_fit = (Phi * time_dynamics);

end
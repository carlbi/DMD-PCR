function separate_fg_bg(obj, dl)

if nargin < 2
    if isempty(obj.Y)
        dl = false;
    else
        dl = true;
    end
end

[dt, t, tol] = unpack(config(), ["dt", "t", "tol"]);
r = numel(obj.DMD_eigenvalues);

omegas = log(obj.DMD_eigenvalues)/dt;
Phi = obj.DMD_Modes;

disp('Separating foreground and background...')

bg = find(abs(omegas)<tol); % Threshold for background separation
fg = setdiff(1:r, bg);
omega_fg = omegas(fg); % foreground
Phi_fg = Phi(:,fg); % DMD foreground modes
omega_bg = omegas(bg); % background
Phi_bg = Phi(:,bg); % DMD background mode

if dl
    b = Phi \ obj.Y(:, 1); % amplitude
else
    b = Phi \ obj.X(:, 1); % amplitude
end

b_fg = b(fg);
b_bg = b(bg);

% Foreground
% b_fg = Phi_fg \ obj.Y(:, 1); % amplitude
X_fg = zeros(numel(omega_fg), length(t));
for tt = 1:length(t)
    X_fg(:, tt) = b_fg .* exp(omega_fg .* t(tt));
end
X_fg = real(Phi_fg * X_fg);
X_fg = X_fg(1:end, :);

% Other reconstruction technique


% Background
% b_bg = Phi_bg \ obj.Y(:, 1); % amplitude
X_bg = zeros(numel(omega_bg), length(t));
for tt = 1:length(t)
    X_bg(:, tt) = b_bg .* exp(omega_bg .* t(tt));
end
X_bg = real(Phi_bg * X_bg);
X_bg = X_bg(1:end, :);

% Save in object
if dl
    obj.Y_fg = X_fg;
    obj.Y_bg = X_bg;
else
    obj.X_fg = -X_fg;
    obj.X_fg(obj.X_fg < 0) = 0;
    obj.X_bg = X_bg;
    obj.X_bg(obj.X_bg < 0) = 0;
end

end
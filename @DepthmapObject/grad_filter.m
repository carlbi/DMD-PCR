function [grad_mask, depthmap_filtered] = grad_filter(dep, grad_cut)

% Compute gradient
[Gmag, ~] = imgradient(dep,'prewitt');
% Convert depthmap
dep = im2double(dep);
% Mask depthmap
grad_sort = sort(Gmag(:));
thresh = grad_sort(ceil(grad_cut*numel(Gmag)));
grad_mask = Gmag<thresh;
depthmap_filtered = grad_mask.*dep;

end


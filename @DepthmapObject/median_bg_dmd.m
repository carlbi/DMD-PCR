function X_bg = median_bg_dmd(X)

[t, dt, tol] = unpack(config(), ["t", "dt", "tol"]);

[Phi, lambdas] = DepthmapObject.dmd(X, 1);
omegas = log(lambdas)/dt;

% Check if background is found
if ~(abs(omegas)<tol); error('No background mode found'); end

% Compute DMD Background Solution
b_bg = Phi \ median(X,2); % for more accurate background estimation
X_bg = Phi * b_bg;
X_bg = repmat(X_bg(1:size(X,1), :), 1,length(t));

end
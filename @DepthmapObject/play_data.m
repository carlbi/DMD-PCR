function play_data(obj, data, scaler)

if nargin < 3
    scaler = 1;
end

[n_frames, fps] = unpack(config(), ["n_frames", "fps"]);
x = obj.size_y; % switch due to Matlab conventions
y = obj.size_x; % switch due to Matlab conventions

possible = ["RGB", "X", "X_fg", "X_bg", "X_dmd", ...
    "R", "R_fg", "R_bg", "R_dmd", "Mx_fg", "Mr_fg"];
if find(strcmp(possible, data)) 
    X_play = obj.(data);
else
    error('Unsure which data to play')
end

if scaler == -1
    implay(rescale(real(reshape(X_play, [x, y, n_frames]))), fps);
else
    implay(scaler*real(reshape(X_play, [x, y, n_frames])), fps);
end

end
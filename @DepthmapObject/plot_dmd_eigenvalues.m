function plot_dmd_eigenvalues(obj)

dt = unpack(config(), "dt");
omegas = log(obj.DMD_eigenvalues)/dt;

figure
plot(omegas, 'o')
title('Continuous time DMD eigenvalues')

end
function run_dmd(obj, dl)

if nargin < 2
    if isempty(obj.Y)
        dl = false;
    else
        dl = true;
    end
end

disp("Dynamic mode decomposition...")

if dl
    [Phi, lambdas] = obj.dmd(obj.Y, obj.DMD_truncation);
else
    [Phi, lambdas] = obj.dmd(obj.X, obj.DMD_truncation);
end

obj.DMD_Modes = Phi;
obj.DMD_eigenvalues = lambdas;

if dl
    [~, Y_dmd_ls_fit] = obj.reconstruct_dmd(Phi, lambdas, obj.Y); 
    obj.Y_dmd = Y_dmd_ls_fit;
else
    [~, X_dmd_ls_fit] = obj.reconstruct_dmd(Phi, lambdas, obj.X); 
    obj.X_dmd = X_dmd_ls_fit;
end

end
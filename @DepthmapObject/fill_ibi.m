function X_out = fill_ibi(X, sizes, s)
% Iterative Background Imputation

if nargin < 3
   s = 25; 
end

n_frames = unpack(config(), "n_frames");

% Init background
X_bg = DepthmapObject.median_bg_dmd(X);
X_bg = repmat(X_bg(:,1),1,n_frames);
% X_out = X; % store X

% Iteratively fill missing values with background
disp('Iterative background imputation...')
for i = progress(1:2*s)
    missing = find(X==0);
    fill_missing = missing(randperm(numel(missing),round(numel(missing)/s)));
    X(fill_missing) = X_bg(fill_missing);
    X_bg = DepthmapObject.median_bg_dmd(X);
end

% Matrix completion for the rest
X_out = DepthmapObject.fill_mc(X, sizes, X_bg);
    
end
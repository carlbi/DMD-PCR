function X_out = prepare_rgb(filepath, sizes)

[size_y, size_x] = unpack(sizes);
[n_frames, compression] = unpack(config(), ["n_frames", "compression"]);

% Read data
filePattern = fullfile(filepath, '*.png');
pngFiles = dir(filePattern);
frames = zeros(1242,2208,n_frames);
disp('Reading images from files...')
for f = progress(1:n_frames)
    fullFileName = fullfile(filepath, pngFiles(f).name);
    current_frame = imread(fullFileName);
    if ndims(current_frame) == 3 % convert rgb to bw
        current_frame = 2^16*im2double(rgb2gray(current_frame));
    end
    frames(:,:,f) = current_frame;
end

% Downsampling
size_x0 = size(frames,2)/compression;
size_y0 = size(frames,1)/compression;
if size_y0 ~= size_y
    error('Incompatible sizes')
end
n = size_x0*size_y0;

X = zeros(n, n_frames);
skip = 1;
%disp('Compressing data...')
for i = (1:skip:size(frames,3))
    frame_bw = frames(1:compression:end,1:compression:end,i)/(2^16);
    X(:,i) = frame_bw(:);
end

% Removing cols from the left
n = size_x*size_y;
remove_col = size_x0-size_x;
X_out = zeros(n,n_frames);
for i = (1:n_frames)
    frame_to_cut = reshape(X(:,i), size_y0, size_x0);
    frame_to_cut(:,1:remove_col) = [];
    X_out(:,i) = frame_to_cut(:);
end

end


function load_view(obj, view, fill)

if nargin < 3; fill = ""; end

[dep, rgb, calib, compression] = unpack(config(), ...
    ["dep", "rgb", "calib", "compression"]);

% Load view
[X_temp, sizes] = obj.prepare_dep(dep(view));
RGB = obj.prepare_rgb(rgb(view), sizes);

% Load calibration
K = readmatrix(calib(view))/compression;

% Fill missing data
if fill == "MC"
    X = obj.fill_mc(X_temp, sizes);
elseif fill == "IBI"
    X = obj.fill_ibi(X_temp, sizes);
else
    warning("No known imputation method for missing data specified")
    X = X_temp;
end

if size(RGB) ~= size(X); error('Size mismatch after loading'); end

% Store in object
obj.size_y = sizes(1);
obj.size_x = sizes(2);
obj.K = K;
obj.RGB = RGB;
obj.X = X;

end
function [X_out, sizes] = prepare_dep(filepath)

[n_frames, compression] = unpack(config(), ["n_frames", "compression"]);

% Read data
filePattern = fullfile(filepath, '*.png');
pngFiles = dir(filePattern);
frames = zeros(1242,2208,n_frames);
disp('Reading depthmaps from files...')
for f = progress(1:n_frames)
    fullFileName = fullfile(filepath, pngFiles(f).name);
    current_frame = imread(fullFileName);
    if ndims(current_frame) == 3 % convert rgb to bw
        current_frame = 2^16*im2double(rgb2gray(current_frame));
    end
    frames(:,:,f) = current_frame;
end

% Downsampling
size_x0 = size(frames,2)/compression;
size_y = size(frames,1)/compression;
n = size_x0*size_y;

X = zeros(n, n_frames);
skip = 1;
%disp('Compressing data...')
for i = (1:skip:size(frames,3))
    frame_bw = frames(1:compression:end,1:compression:end,i)/(2^16);
    X(:,i) = frame_bw(:);
end

% Eliminate zero cols from rectification
remove_col = 0;
disp('Find empty columns from rectification...')
for i = 1:n_frames
    idx = max(find(sum(reshape(X(:,i), [size_y, size_x0]),1)==0));
    if idx
        remove_col = max(remove_col, idx);
    end
end
if remove_col > size_x0/2
    warning('Data might contain a missing column that is not from rectification')
    remove_col = floor(size_x0/10);
end

fprintf('Removing %d empty columns...\n', remove_col)
size_x = size_x0-remove_col;
n = size_x*size_y;
X_out = zeros(n,n_frames);
for i = (1:n_frames)
    frame_to_cut = reshape(X(:,i), size_y, size_x0);
    frame_to_cut(:,1:remove_col) = [];
    X_out(:,i) = frame_to_cut(:);
end

sizes = [size_y, size_x];

end


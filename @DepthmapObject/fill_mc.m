function X_out = fill_mc(X, sizes, X_bg)
% Matrix completion

[size_y, size_x] = unpack(sizes);
n_frames = unpack(config(), "n_frames");

if nargin < 3
    X_bg = X;
end

X_bg = reshape(median(X_bg,2), [size_y, size_x]); % median = (:,1), if X_bg given

% Matrix completion for fill values
disp('Solve matrix completion problem...')
mu = 2;
filter_size = floor(size(X_bg)/10)*2+1;
x0 = medfilt2(X_bg, filter_size, 'symmetric');
opts.maxIts = 100;
opts.printEvery = 0;
omega_mask = DepthmapObject.grad_filter(reshape(X_bg, size_y, size_x), 0.9).*(X_bg~=0);
omega = find(sparse(omega_mask));
observations = X_bg(omega);
X_lr = solver_sNuclearBP({size(X_bg,1),size(X_bg,2),omega}, observations, mu, x0, [], opts);
X_fill = repmat(X_lr(:), 1, n_frames);

% Find pixels to replace 
disp('Replace missing values, outliers and high-gradient noise...')
full_mask = zeros(size(X));
for j = progress(1:n_frames)
    grad_mask = DepthmapObject.grad_filter(reshape(X(:,j), [size_y, size_x]), 1); % grad mask
    full_mask(:,j) = grad_mask(:).*(X(:,j)~=0); % zero mask
end
outliers = isoutlier(X, 'ThresholdFactor', 3); % outlier mask
full_mask = ~outliers.*full_mask;

% Replace
X_out = X; X_out(~full_mask) = X_fill(~full_mask);

end
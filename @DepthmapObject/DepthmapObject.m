classdef DepthmapObject < handle
    properties
        % Size of depthmap pixel coordinates
        size_y
        size_x
        % Intrinsic calibration 
        K
        % RGB data
        RGB
        % Original data
        X
        X_fg
        X_bg
        X_dmd
        % Coefficient matrices
        Y
        Y_fg
        Y_bg
        Y_dmd
        % Reconstructed data
        R
        R_fg
        R_bg
        R_dmd
        % Mask
        Mr_fg
        Mx_fg
        % DMD modes and frequencies
        DMD_Modes
        DMD_eigenvalues
        DMD_truncation
    end
    methods
        function obj = DepthmapObject()
            obj.DMD_truncation = unpack(config(), "r");
        end
        load_views(obj, view, fill)
        run_dmd(obj, dl)
        run_raw_dmd(obj)
        stabilize_modes(obj)
        separate_fg_bg(obj, dl)
        separate_raw_fg_bg(obj)
        reconstruct_from_coeff(obj, Dict)
        generate_fg_mask(obj, dl, scheme, filter)
        % Plotting
        play_data(obj, data, scale)        
        plot_dmd_eigenvalues(obj)
    end
    methods (Static)
        % Load from files
        [dep, sizes]  = prepare_dep(filepath)
        [image] = prepare_rgb(filepath, sizes)
        % Preprocessing
        [dep_filled, dep_bg] = fill_mc(dep, sizes, background)
        [dep_filled, dep_bg] = fill_ibi(dep, sizes)
        % DMD
        [Phi, lambda] = dmd(X, r, dt)
        [X_dmd, b] = reconstruct_dmd(Phi, lambda, X) 
        dep_bg = median_bg_dmd(dep)
        [D, W, V] = computeDWV(Y, Q, Theta, r, phi)
        Phi = Phi_vandermonde(x, n)
        R = R_vandermonde(x, n)
        % Filtering
        [grad_mask, depthmap_filtered] = grad_filter(dep, grad_cut)
        % Plotting
        plot_pointcloud(depthmap)
    end
end
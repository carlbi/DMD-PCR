function generate_fg_mask(obj, dl, scheme, n_std)

if nargin < 2
    if isempty(obj.Y)
        dl = false;
    else
        dl = true;
    end
    scheme = "t-and";
    n_std = 2;
elseif nargin < 3
    scheme = "t-and";
    n_std = 2;
elseif nargin < 4
    n_std = 2;
end

[n_frames, filter] = unpack(config, ["n_frames", "filter"]);

if dl
    thresh = n_std*mean(std(obj.R, [], 2));
    M_fg_diff = (obj.R_bg-obj.R)>thresh;
    M_fg_re = obj.R_fg>thresh;
else
    thresh = n_std*mean(std(obj.X, [], 2));
    M_fg_diff = (obj.X_bg-obj.X)>thresh;
    M_fg_re = obj.X_fg>thresh;
end

switch scheme 
    case "t-and"
        mask = M_fg_diff .* M_fg_re;
    case "bg-t"
        mask = M_fg_diff;
    case "fg-t"
        mask = M_fg_re;
    otherwise
        error("Unknown masking scheme")
end

disp('Generating foreground mask...')

if filter
    for i = progress(1:n_frames)
        temp = medfilt2(reshape(mask(:,i), [obj.size_y, obj.size_x]), [3, 3]);
        mask(:,i) = temp(:);
    end
end

if dl
    obj.Mr_fg = mask;
else
    obj.Mx_fg = mask;
end
    
end
function plot_pointcloud(D)

n_frames = unpack(config(), "n_frames");

player = pcplayer([-2500 2500],[-2500 2500],[-2500 2500]);
for j = 1:n_frames
    if ~isOpen(player)
        break
    end
    PC = PointcloudObject.dep2pcl(...
        reshape(D.X(:,j), [D.size_y, D.size_x]), ...
        reshape(D.RGB(:,j), [D.size_y, D.size_x]),...
        D.K);
    view(player,PC);   
    pause(0.05)
end

end

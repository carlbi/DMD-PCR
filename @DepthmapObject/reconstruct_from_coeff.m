function reconstruct_from_coeff(obj, Dict)

if isempty(obj.Y)
    error('No coefficient matrix available for reconstruction')
end

disp('Reconstructing...')
R = Dict.reconstruct_frames(obj, "Y");
R_fg = -Dict.reconstruct_frames(obj, "Y_fg");
R_bg = Dict.reconstruct_frames(obj, "Y_bg");
R_dmd = Dict.reconstruct_frames(obj, "Y_dmd");

% Deleting negative values
R(R<0) = 0;
R_fg(R_fg<0) = 0;
R_bg(R_bg<0) = 0;
R_dmd(R_dmd<0) = 0;

% Storing
obj.R = R;
obj.R_fg = R_fg;
obj.R_bg = R_bg;
obj.R_dmd = R_dmd;

end
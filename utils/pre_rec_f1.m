function [precision, recall, f1] = pre_rec_f1(GT, M_fg);

est = logical(M_fg);
gt = logical(GT);

fp = sum(~gt & est, 'all');
fn = sum(gt & ~est, 'all');
tp = sum(gt & est, 'all');
% tn = sum(~gt & ~est, 'all');

precision = tp/(tp + fp);

recall = tp/(tp + fn);

f1 = 2/((1/precision) + (1/recall));

end

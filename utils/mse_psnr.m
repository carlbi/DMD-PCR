function [mse, psnr] = mse_psnr(I, K)

mse = norm(I-K)/numel(I)^2;
psnr = 20*log10(max(I, [], 'all')^2/sqrt(mse));

end
function rmse = rmse(P1, P2)

if size(P1) ~= size(P2)
    error('Sizes do not match')
end

if size(P1,1) == 3
    rmse = mean(vecnorm(P2-P1,2,1)); % 2-norm, along 1st dom 
elseif size(P1,2) == 3
    rmse = mean(vecnorm(P2-P1,2,2)); % 2-norm, along 2nd dom 
else
    error('Cannot determine dimensions of point set')
end

end
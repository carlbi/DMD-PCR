function GT = compute_gt_small()

D = DepthmapObject;
D.load_view(1, "MC");

[n_frames, X_lims, Y_lims, Z_lims, Circle, radius] = unpack(config(), ...
    ["n_frames", "X_lims", "Y_lims", "Z_lims", "Circle", "radius"]);

GT = zeros(size(D.X));

for i = progress(1:n_frames)
    pc = PointcloudObject.dep2pcl(reshape(D.X(:,i), [D.size_y, D.size_x]), ...
        reshape(D.RGB(:,i), [D.size_y, D.size_x]), D.K);
    fg = (pc.Location(:,1)-Circle(1)).^2 + (pc.Location(:,3)-Circle(2)).^2 < radius^2 & ...
         pc.Location(:,1) > X_lims(1) & pc.Location(:,1) < X_lims(2) & ...
         pc.Location(:,2) > Y_lims(1) & pc.Location(:,2) < Y_lims(2) & ...
         pc.Location(:,3) > Z_lims(1) & pc.Location(:,3) < Z_lims(2);
    GT(:,i) = fg;
end

end
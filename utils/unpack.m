function varargout = unpack(argin, fields)

narginchk(1,2)

if isnumeric(argin)
    nOutputs = numel(argin);
    for k = 1:nOutputs
        varargout{k} = argin(k);
    end
elseif isstruct(argin)
    if ~isstring(fields)
        error('Cannot read desired fields')
    end
    nOutputs = numel(fields);
    for k = 1:nOutputs
        varargout{k} = argin.(fields(k));
    end
else
    error('Failed unpacking: Input argument must be struct or numeric')
end

end
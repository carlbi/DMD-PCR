function Phi = Phi_vandermonde(x, n)
x = x(:);  % Column vector
Phi = ones(length(x), n);
for t = 2:n
    Phi(:, t) = exp(1i*x) .* Phi(:, t-1);  
end

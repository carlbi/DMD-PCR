function l = logistic(x)

l = (1./(1 + exp(1).^(-1*x)));

end
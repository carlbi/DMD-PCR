function [v, g] = smooth_fr(Y, Q, theta, r, phi)
[D, W, V] = computeDWV(Y, Q, theta, r, phi);
R = R_vandermonde(r, size(Y,2));
Phi = Phi_vandermonde(phi, size(Y,2));

v = 0.5*norm(D,2)^2;
g = zeros(size(r));
for i = 1:size(r)
    Oi = zeros(size(V));
    Oi(i,:) = (0:size(R, 2)-1).*[0 R(i,1:end-1)];
    g(i) = -real(trace(D.'*conj(W)*(Oi.*conj(Phi))));
end
% Set background gradient to zero
% [~, max_idx] = max(r);
% g(max_idx) = 0;
end
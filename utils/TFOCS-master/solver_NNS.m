function [x,odata,opts] = solver_NNS(block, Y, Q, Theta, r, phi, lambda)

if nargin < 7
    lambda = 1e-6;
end

opts.alg = 'GRA';
opts.tol = 1e-6;
opts.maxIts = 100;
opts.printEvery = 10;

switch block
    case 'Q'
        f_sm = @(Q)smooth_fq(Y, Q, Theta, r, phi);
        affine = {};
        % h = prox_l1pos(lambda);
        % h = prox_l1(lambda);
        h = {};
        x0 = Q;
    case 'Theta'
        f_sm = @(Theta)smooth_ftheta(Y, Q, Theta, r, phi);
        affine = {};
        h = {};
        x0 = Theta;
    case 'R'
        f_sm = @(r)smooth_fr(Y, Q, Theta, r, phi);
        affine = {};
        opts.L0 = 1e3;
        h = {};
        % h = proj_box(0,1);
        x0 = r;
    case 'Phi'
        f_sm = @(phi)smooth_fphi(Y, Q, Theta, r, phi);
        affine = {};
        opts.L0 = 1e3;
        h = {};
        % h = proj_box(-pi,pi);
        x0 = phi;
    otherwise
        error('Cost function not implemented')
end

[x,odata,opts] = tfocs(f_sm, affine, h, x0, opts);

% TFOCS v1.3 by Stephen Becker, Emmanuel Candes, and Michael Grant.
% Copyright 2013 California Institute of Technology and CVX Research.
% See the file LICENSE for full license information.


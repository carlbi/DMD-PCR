function [v, g] = smooth_ftheta(Y, Q, theta, r, phi)
[D, ~, V] = computeDWV(Y, Q, theta, r, phi);
Theta = exp(1i*theta);
v = 0.5*norm(D,2)^2;
g = -imag(D*V'.*conj(Theta)).*Q;
end
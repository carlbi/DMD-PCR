function [v, g] = smooth_fq(Y, Q, theta, r, phi)
[D, ~, V] = computeDWV(Y, Q, theta, r, phi);
Theta = exp(1i*theta);
v = 0.5*norm(D,2)^2;
g = -real(D*V'.*conj(Theta));
end


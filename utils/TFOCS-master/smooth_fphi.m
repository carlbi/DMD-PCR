function [v, g] = smooth_fphi(Y, Q, theta, r, phi)
[D, W, V] = computeDWV(Y, Q, theta, r, phi);
R = R_vandermonde(r, size(Y,2));
Phi = Phi_vandermonde(phi, size(Y,2));

v = 0.5*norm(D,2)^2;
g = zeros(size(phi));
for i = 1:size(phi)
    Mi = zeros(size(V));
    Mi(i,:) = (0:size(R, 2)-1)*1j;
    g(i) = -imag(trace(D.'*conj(W)*(R.*Mi.*conj(Phi))));
end
end
function GT = compute_gt_large(D)

[n_frames, X_lims, Y_lims, Z_lims] = unpack(config(), ...
    ["n_frames", "X_lims", "Y_lims", "Z_lims"]);

GT = zeros(size(D.X));

for i = progress(1:n_frames)
    pc = PointcloudObject.dep2pcl(reshape(D.X(:,i), [D.size_y, D.size_x]), ...
        reshape(D.RGB(:,i), [D.size_y, D.size_x]), D.K);
    fg = pc.Location(:,1) > X_lims(1) & pc.Location(:,1) < X_lims(2) & ...
         pc.Location(:,2) > Y_lims(1) & pc.Location(:,2) < Y_lims(2) & ...
         pc.Location(:,3) > Z_lims(1) & pc.Location(:,3) < Z_lims(2);
    GT(:,i) = fg;
end

end
function [D, W, V] = computeDWV(Y, Q, theta, r, phi)
l = size(Y,2);

Theta = exp(1i*theta);
R = R_vandermonde(r, l);
Phi = Phi_vandermonde(phi, l);

W = Q.*Theta;
V = R.*Phi;
D = Y-W*V;
end
function R = R_vandermonde(x, n)
x = x(:);  % Column vector
R = ones(length(x), n);
for t = 2:n
    R(:, t) = x .* R(:, t-1);  
end
